package Main.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "short_urls")
public class ShortURL {
    @Id
    private String token;
    private String longUrl;
    private Date createAt;
    private Date expireAt;

    public ShortURL() {
    }

    public ShortURL(String token, String longUrl, Date createAt, Date expireAt) {
        this.token = token;
        this.longUrl = longUrl;
        this.createAt = createAt;
        this.expireAt = expireAt;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLongUrl() {
        return longUrl;
    }

    public void setLongUrl(String longUrl) {
        this.longUrl = longUrl;
    }

    public Date getCreateAt() {
        return createAt;
    }

    public void setCreateAt(Date createAt) {
        this.createAt = createAt;
    }

    public Date getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(Date expireAt) {
        this.expireAt = expireAt;
    }
}
