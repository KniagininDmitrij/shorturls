package Main.request;

public class ShortRequest {
    private String longURL;
    private boolean registered;

    public ShortRequest() {
    }

    public ShortRequest(String longURL, boolean registered) {
        this.longURL = longURL;
        this.registered = registered;
    }

    public String getLongURL() {
        return longURL;
    }

    public void setLongURL(String longURL) {
        this.longURL = longURL;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }
}
