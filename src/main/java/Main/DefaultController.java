package Main;

import Main.model.User;
import Main.model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class DefaultController {

    private static final String REGISTER_FORM_URL = "/register";
    private static final String REGISTER_URL = "/process_register";
    private static final String LOGIN_FORM_URL = "/login";
    private static final String LOGIN_URL = "/process_login";

    @Autowired
    private UserRepository userRepository;

    @RequestMapping("/")
    public String index(){
        return "index";
    }

    @RequestMapping(REGISTER_FORM_URL)
    public String signUp(Model model){
        model.addAttribute("user", new User());
        return "register-form";
    }

    @PostMapping(REGISTER_URL)
    public String registerProcess(User user){
        userRepository.save(user);
        return "register-success";
    }

    @GetMapping(LOGIN_FORM_URL)
    public String login(Model model){
        model.addAttribute("userLogin", new User());
        return "login-form";
    }

    @PostMapping(LOGIN_URL)
    public String loginProcess(User userLogin){
        //todo сделать проверку на наличие пользователя и правильность пароля
        User user = userRepository.getUserByEmail(userLogin.getEmail());
        if (user == null){
            return "user-not-find";
        }
        System.out.println(user.getFirstName());
        //todo при успешном входе подсаживать куки
        return "index";
    }
}
