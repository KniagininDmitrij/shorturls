package Main;

import Main.model.User;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
    public static final String ROOT_USER_URL = "/user/";
    public static final String LOGIN_URL = "login/";

    @PostMapping(ROOT_USER_URL + LOGIN_URL)
    public User login(){
        return new User();
    }
}
