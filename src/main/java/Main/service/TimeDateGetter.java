package Main.service;

import Main.simple.TimeBounds;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Component
public class TimeDateGetter {
    private static final long DEFAULT_EXPIRE_TIME = 3600*1000*24*3;
    private static final Map<String, Integer> TIME_COEFFICIENTS = new HashMap<>(){{
        put("seconds", 1000);
        put("minutes", 60*1000);
        put("hours", 60*60*1000);
        put("days", 24*60*60*1000);
        put("weeks", 7*24*60*60*1000);
    }};

    private Map<String, Integer> unregisteredExpireMap;
    private Map<String, Integer> registeredExpireMap;
    private long unregisteredExpireTime;
    private long registeredExpireTime;

    ConfigReader configReader = new ConfigReader();

    TimeDateGetter(){
       initialise();
    }

    private void initialise() {
        unregisteredExpireMap = configReader.getUnregisteredExpireTimeMap();
        unregisteredExpireTime = calculateExpireTime(unregisteredExpireMap);
        registeredExpireMap = configReader.getRegExpireTimeMap();
        registeredExpireTime = calculateExpireTime(registeredExpireMap);
    }

    private long calculateExpireTime(Map<String, Integer> expireMap) {
        if (expireMap.isEmpty()){
            return DEFAULT_EXPIRE_TIME;
        }
        long expireTime = 0L;
        for (String key : TIME_COEFFICIENTS.keySet()){
            expireTime += (long) expireMap.getOrDefault(key, 0) * (long) TIME_COEFFICIENTS.get(key);
        }
        return  expireTime;
    }

    public TimeBounds getTimeBounds(boolean registered){
        Date now = new Date();
        Date expireDate = new Date(now.getTime() +
                (registered ? registeredExpireTime : unregisteredExpireTime));
        return new TimeBounds(now, expireDate);
    }
}
