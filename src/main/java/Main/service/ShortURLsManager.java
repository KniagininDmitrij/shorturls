package Main.service;

import Main.model.ShortURL;
import Main.request.ShortRequest;
import Main.simple.TimeBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;

@Component
public class ShortURLsManager {

    @Autowired
    TimeDateGetter timeDateGetter;
    @Autowired
    TokenGenerator tokenGenerator;

    public ShortURL getShortUrlFromRequest(ShortRequest shortRequest){
        ShortURL shortURL = new ShortURL();
        shortURL.setLongUrl(shortRequest.getLongURL());
        TimeBounds timeBounds = timeDateGetter.getTimeBounds(shortRequest.isRegistered());
        shortURL.setCreateAt(timeBounds.getStartDateTime());
        shortURL.setExpireAt(timeBounds.getEndDateTime());
        shortURL.setToken(tokenGenerator.getToken(shortRequest.getLongURL()));
        return shortURL;
    }

    public ResponseEntity getRedirectResponse(ShortURL shortURL) throws URISyntaxException {
        URI longURL = new URI(shortURL.getLongUrl());
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(longURL);
        return new ResponseEntity<>(httpHeaders, HttpStatus.resolve(302));
    }
}
