package Main.service;

import org.hashids.Hashids;
import org.springframework.stereotype.Component;

@Component
public class TokenGenerator {

    public String getToken(String longURL) {
        //todo проработать алгоритм генерации токенов (управление длиной)
        Hashids hashids = new Hashids(longURL);
        String id = hashids.encode(1, 2, 3);
        return id;
    }
}
