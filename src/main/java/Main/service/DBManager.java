package Main.service;

import Main.model.ShortURL;
import Main.model.ShortUrlsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DBManager {

    @Autowired
    ShortUrlsRepository shortUrlsRepository;

    public ShortURL saveShortURL(ShortURL shortURL){
        return shortUrlsRepository.save(shortURL);
    }
}
