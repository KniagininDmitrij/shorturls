package Main.service;

import Main.Main;
import org.springframework.boot.SpringApplication;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.Yaml;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Component
public class ConfigReader {

    private static final String CONFIG_PATH = "application.yml";
    private static Map<String, Object> configMap = new HashMap<>();;

    public ConfigReader(){
        if (configMap.isEmpty()) {
            Yaml yaml = new Yaml();
            try (InputStream configFileInputStream = new FileInputStream(CONFIG_PATH)) {
                configMap = yaml.load(configFileInputStream);
            } catch (IOException e) {
                configMap = new HashMap<>();
                e.printStackTrace();
            }
        }
    }

    public String getPortOrDefault(String defaultPort){
        try {
            return ((Map<String, String>)(configMap.get("server"))).get("port");
        }catch (Exception exception){
            return defaultPort;
        }
    }

    public Map<String, Integer> getUnregisteredExpireTimeMap(){
        try{
            return (Map<String, Integer>) configMap.get("expireUnregistered");
        }
        catch (Exception exception){
            return new HashMap<>();
        }
    }

    public Map<String, Integer> getRegExpireTimeMap() {
        try{
            return (Map<String, Integer>) configMap.get("expireRegistered");
        }
        catch (Exception exception){
            return new HashMap<>();
        }
    }
}
