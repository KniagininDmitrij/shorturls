package Main;

import Main.model.ShortURL;
import Main.model.ShortUrlsRepository;
import Main.request.ShortRequest;
import Main.service.DBManager;
import Main.service.ShortURLsManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

@RestController
public class ShortUrlsController {
    private static final String ROOT = "/";
    public static final String ADD_ROOT = "/short/";

    @Autowired
    ShortUrlsRepository shortUrlsRepository;
    @Autowired
    ShortURLsManager shortURLsManager;
    @Autowired
    DBManager dbManager;

    @GetMapping(ROOT + "{token}")
    public ResponseEntity<ShortURL> get(@PathVariable("token") String token) throws URISyntaxException {
        Optional<ShortURL> optionalShortURL = shortUrlsRepository.findById(token);
        if (optionalShortURL.isPresent()){
            ShortURL shortURL = optionalShortURL.get();
            if (System.currentTimeMillis() < shortURL.getExpireAt().getTime()){
                return shortURLsManager.getRedirectResponse(shortURL);
            }else{
                return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
            }
        }else
            return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @PostMapping(ADD_ROOT)
    public ResponseEntity<ShortURL> add(@RequestBody ShortRequest shortRequest){
        try {
            ShortURL shortURL = shortURLsManager.getShortUrlFromRequest(shortRequest);
            shortURL = dbManager.saveShortURL(shortURL);
            return new ResponseEntity<>(shortURL, HttpStatus.OK);
        }catch (Exception exception){
            return new ResponseEntity(HttpStatus.BAD_REQUEST);
        }
    }
}
