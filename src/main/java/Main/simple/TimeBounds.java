package Main.simple;

import java.util.Date;

public class TimeBounds {
    private Date startDateTime;
    private Date endDateTime;

    public TimeBounds(Date startDateTime, Date endDateTime) {
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
    }

    public Date getStartDateTime() {
        return startDateTime;
    }

    public Date getEndDateTime() {
        return endDateTime;
    }
}
