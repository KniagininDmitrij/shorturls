$(function(){
    $('#short-form').click(function()
    {
        try{
            let formData = new FormData(document.querySelector('#shorting-form form'));
            // Собираем данные формы в объект
            let obj = {};
            formData.forEach((value, key) => obj[key] = value);
            console.log(obj);
            $.ajax({
            method: "POST",
            url: '/short/',
            contentType : 'application/json',
            data: JSON.stringify(obj),
            success: function(response)
            {
                console.log(response["token"]);
                let resultBlock = document.querySelector('#result-link');
                let ref = resultBlock.getElementsByTagName("a")[0];
                var newLink = 'http://localhost:8083/' + response["token"];
                ref.href = newLink;
                ref.textContent = newLink;
                console.log(ref);
                resultBlock.style.visibility='visible';
            }
          });
            return false;
        }catch (e){
            console.log(e);
        }
        return false;
    });
});